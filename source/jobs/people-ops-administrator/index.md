---
layout: markdown_page
title: "People Operations Administrator"
---

## Responsibilities

* Assist in People Operations, specifically:
   * Prepare contracts and help ensure smooth onboarding of new team members, with
   a focus on US-based team members.
   * Handle majority of the administrative interactions with our payroll and benefits
   provider, TriNet.
   * Process changes to team members compensation, position, etc.
* Provide assistance to the team with miscellaneous support tasks. 
