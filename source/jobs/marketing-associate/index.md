---
layout: markdown_page
title: "Marketing Associate"
---

You are an entry-level technical marketer with an urge to learn everything there is to know about the developer marketing world. Every day will be different but you’ll learn about every aspect of technical marketing from swag to metrics.

## Responsibilities

* On the 22nd of every month you know it is time for a beautifully designed newsletter to go out to our community and you are ready to click send at the time of launch. You’ve worked with the demand generation manager, developer marketing manager, and technical team to ensure this newsletter is relevant, beautifully designed, and high quality.  
* Help with event logistics in support of the team. From helping to book space for meetups to making sure the booth is staffed, you can make sure every aspect of our events are well organized.
* You will work closely with the developer marketing manager and engineering team to make sure blog posts are technically and grammatically correct. You’ll also be in charge of posting at the appropriate time and making sure that social media content is tailored for each platform.
* Help the team make website updates to copy, tutorials, and the blog post and track any issues with the website that need marketing or design’s attention.
* Manage incoming emails to the marketing alias and know how to properly route.

## Requirements for Applicants
(Check our [Jobs](https://about.gitlab.com/jobs/) page to see current openings).

* GitLab is a developer-focused open source product so an understanding of the developer product space is required.
* Is your college degree in french foreign politics with a minor in interpretive dance but you’ve been hacking together websites since you were 12? Perfect. We understand that your college degree isn’t the only thing that prepares you as a potential job candidate.
* You are obsessed with metrics, optimization, and graphs that point up and to the right. Are you consistently seeing that a type of content isn’t getting many views? As long as you know to change it up or replace that type of content, this is your role.
* Be ready to learn how to use GitLab and Git as you’ll be able to edit the website directly from terminal.
* Work remotely from anywhere in the world (Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/)!)
