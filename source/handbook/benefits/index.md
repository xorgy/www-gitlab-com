---
layout: markdown_page
title: "Benefits"
---

* [General Benefits](#general-benefits)
* [Regular compensation](#regular-compensation)
* [Specific to Netherland based employees](#nl-specific-benefits)
* [Specific to US based employees](#us-specific-benefits)

## General Benefits <a name="general-benefits"></a>

1. GitLab will pay for the items listed under [spending company money](https://about.gitlab.com/handbook/#spending-company-money).
1.  Deceased team member
    In the unfortunate event that a GitLab team member passes away, GitLab will
    provide a $20,000 lump sum to anyone of their choosing. This can be a spouse,
    partner, family member, friend, or charity.
      * For US based employees of GitLab Inc., this benefit is replaced by the
        [Basic Life Insurance](#basic-life-ins) that is offered through TriNet.
      * For all other team members, the following conditions apply:
         * The team member must have indicated in writing to whom the money
           should be transferred.
         * For part-time team members, the lump sum is calculated pro-rata, so
           for example for a team member that works for GitLab 50% of the time,
           the lump sum would be $5,000.
1. Our [paid time off policy](https://about.gitlab.com/handbook/#paid-time-off)
1. There are [incentives](https://about.gitlab.com/handbook/#incentives) available.

## Regular compensation <a name="regular-compensation"></a>

1. Employees of our Dutch entity (GitLab B.V.) will get their salary wired on the 25th of every month, and can see their payslip in their personal portal on [HR Savvy's system](https://hr-savvy.nmbrs.nl/).
1. Employees of our US entity (GitLab Inc.) have payroll processed semi-monthly through TriNet, and they can access their pay slips through the [TriNet portal](https://www.hrpassport.com).
1. Contractors to GitLab (either entity) should send their invoices for services rendered to ap@gitlab.com
   * For 'fixed fee' contracts, it is OK to send the invoice before the time period
   that it covers is over. For example, an invoice covering the period of March 1-31 can be sent on March 25.
   * All invoices are internally reviewed, approved, and then payment is processed.
   This is usually a fast process, but be aware that it can incur delays around vacations.

## Specific to employees based in the Netherlands <a name="nl-specific-benefits"></a>

Dutch employees get the customary month of vacation money in the month of May.

## Specific to US based employees <a name="us-specific-benefits"></a>

US based employees' payroll and benefits are arranged through TriNet. The most up
to date and correct information is always available to employees through the
[TriNet HRPassport portal](https://www.hrpassport.com) and the various contact forms
and numbers listed there. This brief overview is not intended to replace the
documentation in TriNet, but rather to give our team members and applicants a
quick reference guide.

### Medical

TriNet partners with leading carriers, like Aetna, Florida Blue, Blue Shield of
California and Kaiser, to offer a broad range of medical plans, including high
deductible health plans, PPOs and HMOs. Medical plan options vary by state, and
may also include regional carriers.

### Dental

TriNet's four dental carriers -Delta Dental, Aetna, Guardian Dental, and MetLife-
offer a high and a low national dental PPO plan. Aetna and Delta Dental also
will make available a DMO plan in many states.

### Vision

TriNet also offers a high and a low vision plan nationally through two different
carriers: Aetna and Vision Service Plan (VSP). These plan options ensure that
you can choose the best plan and carrier for your individual vision needs.

The Company pays 100% of premiums for medical, 100% of premiums for dental and
100% of premiums for vision coverage for employee. In addition, the Company
funds 50% of premiums for medical, 50% of premiums for dental and 50% of premiums
for vision coverage for spouse, dependent and domestic partner. These contribution
amounts are capped at:

| Insurance                          | Company Contribution Cap |
| ---------------------------------- | -----------------------: |
| Medical Employee Only              |                  $467.00 |
| Medical Employee + Spouse          |                  $747.00 |
| Medical Employee + Child(ren)      |                  $700.50 |
| Medical Employee + Family          |                  $934.00 |
| Group Dental Employee Only         |                   $19.80 |
| Group Dental Employee + Spouse     |                   $30.20 |
| Group Dental Employee + Child(ren) |                   $31.19 |
| Group Dental Employee + Family     |                   $41.59 |
| Group Vision Employee Only         |                    $9.15 |
| Group Vision Employee + Spouse     |                   $13.75 |
| Group Vision Employee + Child(ren) |                   $14.39 |
| Group Vision Employee + Family     |                   $20.26 |

---

You are responsible for the remainder of the premium cost, if any.

If you already have current group medical coverage, you may choose to waive or
opt out of TriNet's group health benefits. If you choose to waive health coverage,
you will receive a $300.00 monthly benefit allowance and will still be able to
enroll in optional plans and flexible spending accounts.

### Basic Life Insurance & AD&D<a name="basic-life-ins"></a>

TriNet offers company paid basic life and accidental death and dismemberment (AD&D)
plans. The Company pays for basic life insurance coverage valued at $20,000, which
includes an equal amount of AD&D coverage.

### Group Long-Term Disability Insurance

The Company provides a policy that may replace up to 60% of your salary, up to
a maximum benefit of $10,000 per month, for qualifying disabilities. A waiting
period of 180 days will apply.

### 401k Plan

The company offers a 401k plan in which you may make voluntary pre-tax contributions
toward your retirement. We do not currently offer matching contributions.

### Optional TriNet Plans Available at Employee Expense

#### Flexible Spending Account (FSA) Plans

FSAs help you pay for eligible out-of-pocket health care and dependent day care expenses
on a pretax basis. You determine your projected expenses for the Plan Year and then
elect to set aside a portion of each paycheck into your FSA.

#### Supplemental Life Insurance

If you want extra protection for yourself and your eligible dependents, you have
the option to elect supplemental life insurance. You may request coverage yourself
for one to six times your annual salary, with a maximum benefit of $2,000,000.
Spousal coverage is also available in increments of $10,000 up to $150,000, and
child coverage for $10,000. Note that amounts above guaranteed issue
($300,000 for you and $30,000 for your spouse) and certain coverage increases
must be approved by the insurance carrier.

#### Supplemental Accidental Death and Dismemberment Insurance

AD&D covers death or dismemberment from an accident only. You may elect supplemental
AD&D coverage in amounts of $25,000, $50,000, $100,000, $250,000, $500,000 or $750,000.

#### Short and Long-Term Disability Insurance

Disability insurance plans are designed to provide income protection while you recover
from a disability. This coverage not only ensures that you are able to receive some
income while out on disability; it also provides absence management support that helps
facilitate your return to work. TriNet offers several short and longterm disability
plan options.