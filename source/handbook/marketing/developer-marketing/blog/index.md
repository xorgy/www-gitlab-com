---
layout: markdown_page
title: "Developer Marketing - Blogging"
---

The [blog](/blog) is our main publishing outlet. We publish content multiple times a week. These posts include

- Short form articles
- Long form articles
- Release announcements
- Feature highlights
- Tutorials
- Inside GitLab

We ensure there is a weekly blog post. We also want to bring in voices from all throughout the company, as well as from GitLab users and our clients.

## The topics we write about

### Product-specific topics

- Tutorials on using GitLab, GitLab CI, etc.
- Feature highlights bring attention to specific features at GitLab.

### User Stories

* Customer stories 'why we choose GitLab'
* Contributor stories 'why I contribute to GitLab'
* Use case stories 'how we use GitLab'
* Boss stories 'how GitLab enabled innersourcing'
* Inception stories 'how GitLab uses GitLab'
* Adoption stories 'how we switched from SVN to GitLab'

## Community contributions

We are developing a program to invite paid contribution by GitLab community members.
Read more about the [Community Writers](https://about.gitlab.com/community/writers) program.

## Blog post backlog

- Anything not assigned to a person is the [backlog](https://dev.gitlab.org/gitlab/blog-posts/issues?milestone_id=&scope=all&sort=created_desc&state=opened&utf8=%E2%9C%93&author_id=&assignee_id=0&milestone_title=&label_name=)
- Anything that is assigned to a person in 'in progress'
- Anything that has a non-WIP MR is ready for review

## <a name="checklist"></a>Blog post publishing checklist

Before you write, make sure you're on a new branch cloned from master.
Check these before you publish:

- First instance of GitLab should be linked to [GitLab](http://about.gitlab.com)
- Check all links.
- Check the date on the file.
- Check the date in the post.
- Check the image is crunched down. Use [tinypng](tinypng.com).
- Check the blog appears good locally.
