---
layout: markdown_page
title: "Hiring"
---
## On this page:
* [Vacancy Creation Process](#vacancy-creation-process)
* [Hiring Process](#hiring-process)
* [Screening Call](#screening-call)
* [Interview Questions](#interview-questions)
* [Getting Contracts Ready](#prep-contracts)
* [Contract signed: now what?](#move-to-onboarding)

## Vacancy Creation Process<a name="vacancy-creation-process"></a>

The CEO needs to authorize any new job positions/searches, and agree on the proposed hiring team.

1. Define hiring team. Roles can be assigned fluidly (see below for the [Hiring Process](#hiring-process)), depending on who is available, bearing in
mind that the most time consuming aspect tends to be review of the first wave of applicants.    
1. Create the job description on our website, and in Workable
    1. Create the relevant page in `https://about.gitlab.com/jobs/[name-of-job]`
    1. Add a job of the exact same job title on [Workable](https://gitlab.workable.com/backend)
       * For location, select "Telecommute".
       * For the description, simply write `For the job description, see [url of relevant jobs page on GitLab's website]`
       * Indicate what applicants need to provide with their application. By default, this will include their resumé, a cover letter, but it may also
       include qualifying questions such as "What timezone are you in?" and "Are you aware that this is not a DevOps role?".
       * "Publish" the job, and follow the links to the application form.
    1. Embed the link to the application form for the new job on our [Jobs page](https://about.gitlab.com/jobs/)
1. Optional: advertise the job description.
    * This can be through “soft” referral, e.g. all GitLab staff post link to jobs site on their LinkedIn profiles.
    * Tweet the new job posting.
    * Consider advertising and/or listing on paid / free job boards.
    * Use the [Workable Clipper](http://resources.workable.com/the-workable-clipper) to help source candidates directly from LinkedIn, and  familiarize yourself with the Workable environment, work flow, features, and support desk.

## Hiring Process<a name="hiring-process"></a>

1. Confirm application: applicants automatically receive confirmation of their application, thanking them for submitting their information. This is an automated message from Workable. If the person came though another channel please add them to Workable before continuing the process. There are various ways to do this, see [Workable's documentation](https://resources.workable.com/adding-candidates).
1. Ask more information if needed: if information is missing and the applicant seems sufficiently promising (or not enough information to be able to make that determination), the appropriate person from the hiring team should follow up requesting additional information.
1. Hiring manager does a first round of rejections. Disqualified candidates should be sent a note informing them of the rejection. There are templates in Workable to assist, but messages can be tailored as appropriate: place yourself in the receiving end of the message.
1. [Screening call](#screening-call) (optional, see below for further detail).
1. Technical interview (optional): As described on the [Jobs](https://about.gitlab.com/jobs/) page, certain positions
require [technical interviews](https://about.gitlab.com/jobs/#technical-interview).
1. Manager interview (see below for questions)
1. C-level executive interview (if different than the manager, see below for questions)
1. CEO interview (if different than the C-level executive, see below for questions)
1. Make a verbal or written (email) offer (the CEO needs to authorize offers)
1. Hiring manager follows up to ensure that the offer is accepted, and then moves to [preparing contracts](#prep-contracts)
1. Hiring manager ensures that the contract is signed, and [starts the onboarding process](#move-to-onboarding) (the People Ops team can help).

At any time during this process the applicant can be rejected (and should always be notified of this). If the
applicant asks for further feedback always offer frank feedback. This is hard, but it is part of our company values.

## Screening Call<a name="screening-call"></a>

For some positions, we conduct screening calls. This call is typically done by our [administrative coordinator](https://about.gitlab.com/jobs/administrative-coordinator/).

Questions are:

1. Why are they looking for a new job?
1. What is your experience with X? (do for each of the skills asked in the job description)
1. How do they feel about working remotely and do they have experience with it?
1. Compensation expectation and compensation in current/last job.

[An example of the output of a good screening call](https://gitlab.workable.com/backend/jobs/128446/browser/applied/candidate/7604850) (need workable account).

At the end of the screening call applicant should be told what the timeline is for what the next steps are (if any).
An example message would be "We are reviewing applications through the end of next week, and will let you know by the end of two weeks from today whether you've been selected for the next round or not. Please feel free to ping us if you haven't heard anything from us by then."

## Interview Questions<a name="interview-questions"></a>

Note: So you are about to interview folks for a job at GitLab? Please take a moment to carefully read
[this document on keeping it relevant and legal, including a self-test](https://docs.google.com/document/d/1JNrDqtVGq3Y652ooxrOTr9Nc9TnxLj5N-KozzK5CqXw).

1. Do you have any questions about the job or GitLab?
1. Why apply to GitLab?
1. For each significant study and job ask: why did you select this one and why did it end? (for studies: why not do master, PhD, PostDoc, etc.)
1. What did you like least about your last position?
1. When were you most satisfied in your position?
1. Take each skill required for the job and do a [STAR](https://en.wikipedia.org/wiki/Situation,_Task,_Action,_Result) for a couple of situations.
1. What professional achievements are you most proud of?
1. How do you keep up to date with developments in your profession?
1. If you don't get this job what will you do?
1. How can we change GitLab the product or company to make it better?
1. What do you expect to achieve in your first month at GitLab?
1. Where do you want to be in three years from now?
1. Have you worked remote before and how do you feel about it?
1. What salary range would you feel comfortable with?
1. If you get hired when can you start?
1. Are you interviewing anywhere else?
1. Do you have any questions for me?

## Getting Contracts Ready<a name="prep-contracts"></a>

Once a new team member is to be added in an employee or contractor capacity,
fill out the [New Contract form](https://docs.google.com/a/gitlab.com/forms/d/1Cthnkdj_23ev_u7LT01wv5dZYAZKm20vp5JmzT3ECqE/viewform)
with pertinent details, and ping the Business Office Manager to initiate the creation of the contracts.

Our template contracts are posted on the [Contracts page](https://about.gitlab.com/handbook/contracts).

## Onboarding<a name="move-to-onboarding"></a>

Once the contract has been signed, create an onboarding issue as detailed on the
[General Onboarding](https://about.gitlab.com/handbook/general-onboarding/) page. Important: create the related
onboarding issue as soon as possible.
