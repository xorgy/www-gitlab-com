---
layout: markdown_page
title: "GitLab Strategy"
---

## Why

GitLab was created because Dmitriy needed an affordable tool to collaborate with his team. He wanted something efficient and enjoyable so he could focus on his work, not the tools. He worked on it from home, a house in Ukraine without running water.

We think that it is logical that our collaboration tools are a collaborative work themselves. More than 1000 people have contributed to GitLab to make that a reality. We believe in a world where **everyone can contribute**. Allowing everyone to make a proposal is the core of what a DVCS (Distributed Version Control System) such as Git enables. No invite needed: if you can see it, you can contribute.

We believe that all digital products should be open to contributions, from legal documents to movie scripts and from websites to chip designs. GitLab Inc. will develop great open source software to enable people to collaborate in this way.

1. To ensure that **everyone can contribute** to digital products we make GitLab joyful to use. It is integrated and opinionated collaboration software. Because there is no need to string together multiple tools you spend less time, have less frustration, there is less [bikeshedding](https://en.wikipedia.org/wiki/Law_of_triviality), and you get more results.

2. To ensure that **everyone can contribute** with GitLab every person in the world should be able to afford it. Therefore GitLab CE is [free as in beer](http://www.howtogeek.com/howto/31717/what-do-the-phrases-free-speech-vs.-free-beer-really-mean/). Also GitLab.com is free with private repos and CI runners, so that even without a budget you can use a great tool.

3. To ensure that **everyone can contribute** using GitLab in some form it should be free as in speech, that is why GitLab CE is MIT licensed. But open source is more than a license, that is why we actively help our competitor Perforce to ship GitLab as part of Perforce Helix. We try to be [a good steward of](https://about.gitlab.com/about/#stewardship) GitLab CE. We keep all our software open to inspection, modifications, enhancements, and suggestions.

4. To ensure that **everyone can contribute** to GitLab itself we actively welcome contributors. We do this by having quality code, tests, documentation, using popular frameworks, and offering a comprehensive GitLab Development Kit. We celebrate contributions by recognizing a Most Valuable Person (MVP) every month. We allow everyone to anticipate, propose, discuss, and contribute features by having everything on a public issue tracker. We ship a new version every month so contributions and feedback are visible fast. To contribute to open source software people must be empowered to learn programming. That is why we sponsor initiatives such as Rails Girls and lean poker.

5. To ensure that **everyone can contribute** to our organization we have open business processes that allow all team members to suggest improvements to our handbook. We hire remotely so everyone with an internet connection can come work for us and be judged on results, not presence in the office. We offer equal opportunity for every nationality. We are agnostic to location and that can be a real benefit to people in countries with less opportunities. We engage on Hacker News, Twitter, and our blog post comments. And we strive to take decisions guided by [our values](https://about.gitlab.com/handbook/#values).

## Goals

1. Ensure that **everyone can contribute** in the 5 ways outlined above.

2. Become most used software for the software development lifecycle and collaboration on all digital content.

3. Complete our product vision of an [opinionated and integrated set of tools based on convention over configuration that offers superior user experience](https://about.gitlab.com/direction/#vision).

4. Offer a sense of progress [in a supportive environment with smart colleagues](http://pandodaily.com/2012/08/10/dear-startup-genius-choosing-co-founders-burning-out-employees-and-lean-vs-fat-startups/).

5. Stay independent long term so we can preserve our values.

## Sequence

1. Become the most popular solution on-premises (DONE)

2. Become the most revenue generation solution on-premises

3. Become the most popular SaaS solution for private repositories

4. Become the most popular SaaS solution for public repositories

5. All knowledge workers contributing via GitLab

## Assumptions

1. [Open source user benefits](http://buytaert.net/acquia-retrospective-2015): significant advantages over proprietary software because of its faster innovation, higher quality, freedom from vendor lock-in, greater security, and lower total cost of ownership.

2. [Open Source stewardship](https://about.gitlab.com/about/#stewardship): community comes first, and share the pie with competitors.

3. [Innersourcing](https://about.gitlab.com/2014/09/05/innersourcing-using-the-open-source-workflow-to-improve-collaboration-within-an-organization/) is needed and will force companies to choose one solution top-down.

4. Open Source model: openness, 10x better value, and help non-paying users.

5. Git will dominate the version control market in 2020.

6. An [integrated tool](https://medium.com/@gerstenzang/developer-tools-why-it-s-hard-to-build-a-big-business-423436993f1c#.ie38a0cls) is superior to a collection of tools or a network of tools.

7. To be sustainable we need an open core model that includes a proprietary GitLab EE.

## Constraints

1. Founder control: vote & board majority so we can keep making long term decisions.

2. Independence: since we took financing, to prevent being acquired, we need to IPO.

3. Low burn: spend seed money like it is the last we’ll raise, maintain 2 years of runway.

4. First time right: last to market so we get it right the first time, a fast follower with taste.

5. Low price: we must have low prices for EE since CE is an anchor and the delta is small.

6. Free SaaS: to make GitLab.com the most popular SaaS in 2020 it should be largely free.

7. Reach: go for a broad reach, no focus on business verticals or certain professions.

8. Speed: ship every change in the next release to maximize responsiveness and learning.

9. Life balance: we want people to stay with us long time, so time off is encouraged.

10. Values: make decisions based on [our values](https://about.gitlab.com/handbook/#values), even if it is inconvenient.

## General 2016

1. Work with our [strategic alliances](https://docs.google.com/a/gitlab.com/document/d/1-oAf0tMlTrAaPAsG_8NLXrI3DEZqI5ZA0gW0lKxFjA4/edit?usp=drive_web) to increase awareness and sales.

2. Partner with Rocket Chat, ship it with GitLab and integrate it.

3. Open up everything we can, including an [open Chef repo](https://dev.gitlab.org/cookbooks/chef-repo/issues/247), and open Asana.

4. Publish our processes and plans in the handbook or link from there.

5. Keep hiring self-directed people that experience the freedom to suggest changes.

6. To preserve culture we should over communicate and regularly discuss our values.

7. Drop minimum basic subscription user count to 1 user.

8. Work on [leadership development](https://docs.google.com/document/d/11XBwnyLZE7ocHSx147Az09XtElzAPR_osc-H75BYkMI/edit) to promote from within where possible.

9. Spread monthly version cadence to operations and maybe marketing.

## Technology 2016

1. Make something people want: merging community contributions is the priority and at least one person will work on that fulltime.

2. Ship the features in our direction [https://about.gitlab.com/direction/](https://about.gitlab.com/direction/)

3. Improve code quality, look and feel, usability, and performance.

4. Implement effective development processes and leadership.

5. Quickly onboard new team members and contributors.

## Revenue 2016

1. Triple ARR (Annual Recurring Revenue) in 2016&17&18, double it in 2019&20.

2. Enable sales to provide more effective product demonstrations, share use case stories and create custom solutions and proposals.

3. Increase revenue globally, with a focus on APAC, by developing channel sales program (value-added reseller and OEM).

4. Increase our average deal size through upsell to higher plans, selling multi-year deals and increasing seats sold.

5. Increase adoption and revenue within accounts through the creation of a Customer Success Organization

6. Increase salesperson capacity and productivity by creating specialized roles: BDR, (senior) account managers, customer success, regional director

7. Account managers specialize in upsell EE from CE, competitive sell against GitHub or Atlassian and educational sell from SVN/Clearcase or top-down sell to CTO.

8. Intense one month onboarding program (GLU, GitLab, Udacity, TrainTool) to prepare new salespeople to add value faster to the buying process.

## Marketing 2016

1. Automation: automation will bring predictable, growing, and well-prioritized leads.

2. PR pitch: [opinionated and integrated set of tools based on CoC offers a superior UX](https://about.gitlab.com/direction/).

3. Needs to win hearts and minds with excellent [developer marketing](https://about.gitlab.com/handbook/marketing/developer-marketing/).

4. Show that we can test big projects (Rails, etc.) faster than Travis CI.

5. Announce we'll sponsor every diversity in tech event outside the US with at least $500.

6. Introduce 'everyone can contribute' as our why and tagline.

7. Hire partner marketing and product marketing people (release posts, website, and sales enablement).

## Finance 2016

1. Globally integrated finance with NetSuite.

2. A great subscription system that is loved by sales.

3. Need enough cash, raise if terms are great.

4. Efficient finance that enables team, sales, etc.

5. Efficient legal counsel with expert people and short cycles.

6. Within 2016 we'll switch from TCV to ARR as the metric for bonuses.

7. Monthly incentive for the whole company ($100 dinner) if we make our sales goal.

## Operations 2016

1. [Respond to all community communication](https://dev.gitlab.org/gitlab/organization/issues/306) and have reliable response times.

2. Include goals and promotion criteria in all job descriptions.

3. Develop great recruiting, hiring, onboarding, offboarding practices with full time HR and recruiter.

4. To keep our great culture we need to institutionalize feedback.

5. Sponsor initiatives and hire in countries that have low opportunity.

## DevOps 2016

1. A highly available GitLab.com that feels fast and has little downtime.

2. [Free CI runners for everyone](https://dev.gitlab.org/gitlab/gitlab-ci/issues/322) for at least as long as our Azure and DO credits last.

3. Foster leadership that stresses radiating knowledge in code, documentation, and blog articles.

4. Continuous Deployment of master to GitLab.com via packages.

5. Solutions for performance measurement, logs, and errors that ship with EE.

## Partnerships 2016

1. Most partnerships are not public but people working at GitLab Inc. can find them in the 'Partnerships' Google Doc.

1. We'll hire a Strategic Relations Manager.

1. The reseller partnerships are especially important here.

1. We also want to get shipped as official Debian package.
